from allauth.socialaccount.models import SocialAccount, SocialToken
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
import requests
from .forms import ImageForm
from .models import Image
import vk_api


def image_upload_view(request):
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            img_obj = form.instance
            # upload_photo()
            # photo = {'photo': open('media/' + str(Image.objects.filter().last().image), 'rb')}
            # upload_url = upload_photo()
            # upload = requests.post(upload_url, files=photo).json()
            # print(upload)
            # response = save_photo(upload)
            vk_session = vk_api.VkApi(token=SocialToken.objects.get(app_id=2).token, api_version='5.131')
            # vk_session.auth(token_only=True)
            vk = vk_session.get_api()
            url = vk.photos.getOwnerPhotoUploadServer()['upload_url']
            req = requests.post(url, files={
                'photo': open(
                    'media/' + str(Image.objects.filter().last().image),
                    'rb')}).json()
            photo = req['photo']
            server = req['server']
            hash = req['hash']
            print(vk.photos.saveOwnerPhoto(server=server, hash=hash, photo=photo))

            # posts = vk.wall.get()
            # post_id = posts["items"][0]["id"]
            # vk.wall.delete(post_id=post_id)

            return render(request, 'upload.html', {'form': form, 'img_obj': img_obj})
    else:
        form = ImageForm()
    return render(request, 'upload.html', {'form': form})


class SignUp(CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("login")
    template_name = "registration/signup.html"


def home(request):
    return render(request, "home.html", {
        "photo_vk": SocialAccount.objects.get(provider="vk").extra_data["photo_max_orig"],

    })


# token = SocialToken.objects.get(app_id=1).token
# owner_id = SocialAccount.objects.get(provider='vk').uid


# photo = {'photo': open(str(Image.objects.filter().last().image), 'rb')}


# def upload_photo():
#     token = SocialToken.objects.get(app_id=1).token
#     owner_id = SocialAccount.objects.get(provider='vk').uid
#     u_url = requests.post('https://api.vk.com/method/photos.getOwnerPhotoUploadServer',
#                           params={
#                               'access_token': token,
#                               'owner_id': owner_id,
#                               'v': 5.131
#                           }).json()
#     return u_url['response']['upload_url']
#
# #
# # photo = {'photo': open('media/' + str(Image.objects.filter().last().image), 'rb')}
# # upload_url = upload_photo()
# # upload = requests.post(upload_url, files=photo).json()
#
#
# def save_photo(upload):
#     token = SocialToken.objects.get(app_id=1).token
#     owner_id = SocialAccount.objects.get(provider='vk').uid
#     save = requests.get('https://api.vk.com/method/photos.saveOwnerPhoto',
#                         params={
#                             'access_token': token,
#                             'owner_id': owner_id,
#                             'server': upload['server'],
#                             'photo': upload['photo'],
#                             'hash': upload['hash'],
#                             'v': 5.131
#                         }).json()
#     return save

# vk_session = vk_api.VkApi(token=SocialToken.objects.get(app_id=1).token, api_version='5.131')
# vk_session.auth(token_only=True)
# vk = vk_session.get_api()
# url = vk.photos.getOwnerPhotoUploadServer()['upload_url']
#
# # photo = []
#
# request = requests.post(url, files={'photo': open('media/' + str(Image.objects.filter().last().image), 'rb')}).json()
# # photo.append(request['photo'])
# photo = request['photo']
# server = request['server']
# hash = request['hash']
# vk.photos.saveOwnerPhoto(server=server, hash=hash, photo=photo)
#
# posts = vk.wall.get()
# post_id = posts["items"][0]["id"]
# vk.wall.delete(post_id=post_id)
