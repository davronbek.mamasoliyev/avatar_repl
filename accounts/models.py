from django.db import models


class Image(models.Model):
    title = models.CharField(max_length=200, blank=True, null=True)
    image = models.ImageField(blank=True, default=None)

    def __str__(self):
        return self.title
